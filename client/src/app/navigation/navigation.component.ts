import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from '../services/current-user.service';
import { User } from '../modules/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  private currentUser: User;
  private typeOfCurrentUser: string;

  constructor(private currentUserService: CurrentUserService,
              private router: Router) {
    currentUserService.getCurrentUser().subscribe((value) => {
      this.currentUser = value;
    });

    currentUserService.getType().subscribe((value) => {
      this.typeOfCurrentUser = value;
    });
   }

  ngOnInit() {
  }

  public logout() {
    this.currentUserService.logout();
    this.router.navigate(['/']);
  }

}
