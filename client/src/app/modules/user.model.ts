import { Pet } from './pet.model';

export interface User {
    _id: string;
    email: string;
    password: string;
    name: string;
    surname: string;
}

export interface Owner extends User {
    address: string;
    pets: Pet[];
}


// tslint:disable-next-line: no-empty-interface
export interface Walker extends User {

}

