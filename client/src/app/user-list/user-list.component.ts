import { Component, OnInit } from '@angular/core';
import { Walker, Owner } from '../modules/user.model';
import { OwnersService } from '../services/owner.service';
import { WalkersService } from '../services/walker.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  public owners: Owner[] = [];
  public walkers: Walker[] = [];
  constructor( private ownerService: OwnersService,
               private walkerService: WalkersService) {
    this.ownerService.getOwners().subscribe((owners: Owner[]) => {
      this.owners = owners;
    });

    this.walkerService.getWalkers().subscribe((walkers: Walker[]) => {
      this.walkers = walkers;
    });
  }

  ngOnInit() {
  }

  public deleteWalker(id: string) {
    if (window.confirm('Are you sure you want delete user?')) {
      this.walkers.filter((walker, index) => {
        if (walker._id === id) {
          this.walkers.splice(index, 1);
        }
      });
      this.walkerService.removeUserById(id).subscribe();
    }
  }

  public deleteOwner(id: string) {
    if (window.confirm('Are you sure you want delete user?')) {
      this.owners.filter((owner, index) => {
        if (owner._id === id) {
          this.owners.splice(index, 1);
        }
      });
      this.ownerService.removeUserById(id).subscribe();
    }
  }


}
