const mongoose = require('mongoose');

const walkersSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true  
    },
    surname: {
        type: String,
        required: true  
    },
    password: {
        type: String,
        required: true  
    },
    email: {
        type: String,
        required: true  
    }
});

const walkersModel = mongoose.model('walkers', walkersSchema);

module.exports = walkersModel;