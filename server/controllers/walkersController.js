const Walker = require('../models/walkersModel');
const mongoos = require('mongoose');

module.exports.getWalkers = async (req, res, next) => {

    try {
        const walkers = await Walker.find({}).exec();
        res.status(200).json(walkers);
    } catch (err) {
        next(err);
    }
};


module.exports.getWalkerById = async (req, res, next) => {

    try {
        const walker = await Walker.findById(req.params.id).exec();

        if (walker) {
            res.status(200);
            res.json(walker);
        } else {
            res.status(404);
            res.send();
        }
    } catch (err) {
        next(err);
    }
};

module.exports.createWalker = async (req, res, next) => {

    try {
        if (!req.body.name || !req.body.surname || !req.body.password || !req.body.email) {
            if (!req.body.email || !req.body.password) {
                if (!req.body.email) {
                    res.status(400);
                    res.send();
                } else {
                    const walker = await Walker.find({ email: req.body.email });
                    if (walker) {
                        res.status(200).json(walker);
                    } else {
                        res.status(400);
                        res.send();
                    }
                }
            } else {
                const walker = await Walker.find({ email: req.body.email, password: req.body.password });
                if (walker) {
                    res.status(200).json(walker);
                } else {
                    res.status(400);
                    res.send();
                }
            }
        } else {
            const newWalker = new Walker({
                _id: new mongoos.Types.ObjectId(),
                name: req.body.name,
                surname: req.body.surname,
                email: req.body.email,
                password: req.body.password
            });

            await newWalker.save();
            res.status(201).json(newWalker);
        }
    } catch (err) {
        next(err);
    }
};

module.exports.updateWalkerById = async (req, res, next) => {

    try {
        const walker = await Walker.findById(req.params.id).exec();
        if (walker) {
            const body = req.body;
            if (body.currentPassword && body.newPassword) {
                if (body.currentPassword != walker.password) {
                    res.status(200).json('false');
                } else {
                    walker.password = body.newPassword;
                    await walker.save();
                    res.status(200).json('true');
                }
            }
        } else {
            res.status(404).send();
        }
    } catch (err) {
        next(err);
    }
};

module.exports.deleteWalkerById = async (req, res, next) => {

    try {
        const walker = await Walker.findById(req.params.id).exec();
        if (walker) {
            await Walker.findByIdAndDelete(req.params.id).exec();
            res.status(200).send();
        } else {
            res.status(404).send();
        }
    } catch (err) {
        next(err);
    }
};