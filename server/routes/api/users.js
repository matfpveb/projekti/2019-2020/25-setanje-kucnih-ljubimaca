const express = require('express');

const router = express.Router();

const controller = require('../../controllers/usersController');

router.get('/', controller.getUsers);
router.get('/:id', controller.getUserById);


router.post('/', controller.createUser);
router.post('/:id', controller.updateUserById);
router.delete('/:id', controller.deleteUserById);


router.post('/:id/pet', controller.addUserPet);
router.delete('/:userId/:petId', controller.removePetById);

router.use((req, res, next) => {
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json({'message': 'Nepoznat zahtev ${unknownMethod} ka ${unknownPath}!'});
});


module.exports = router