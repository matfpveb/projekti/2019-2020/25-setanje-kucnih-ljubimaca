const express = require('express');

const router = express.Router();

const controller = require('../../controllers/requestsController');

router.get('/', controller.getRequests);
router.get('/:id', controller.getRequestById);
router.get('/owner/:id', controller.getRequestForOwner);


router.post('/', controller.createRequest);

router.post('/delete', controller.deleteRequest);
router.post('/:id', controller.updateRequestById);
router.post('/:id/delete', controller.deleteWalkerById);
router.delete('/:id', controller.deleteRequestById);

router.use((req, res, next) => {
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json({'message': 'Nepoznat zahtev ${unknownMethod} ka ${unknownPath}!'});
});


module.exports = router